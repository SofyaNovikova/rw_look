let intervalId = 0;
let hasFreePlaces = false;

function onClick() {
    if (intervalId) {
        clearInterval(intervalId);
    }
    const from = $('#from').val();
    const to = $('#to').val();
    const date = $('#date').val();
    const trains = $('#train').val() ? $('#train').val().split(' ') : [];
    call(from, to, date, trains);
    intervalId = setInterval(call.bind(null, from, to, date, trains), 20000);
};

function call(from, to, date, trains) {
    let url =  `http://rasp.rw.by/ru/route/?from=${from}&from_exp=&from_esr=&to=${to}&to_exp=2400000&to_esr=0&date=${date}`;
    $.ajax({
        type: 'GET',
        url
    })
        .done( function(data) {
            const element = document.createElement('div');
            element.innerHTML = data;
            const table = element.querySelectorAll('.schedule_main')[0];
            const rows = table ? table.querySelectorAll('tr') : [];
            rows.forEach(function (row) {
                const isFreePlaces = row.classList.contains('w_places');
                const train = isFreePlaces ? row.querySelectorAll('.train_id')[0].innerText : '';
                if ( isFreePlaces && trains.indexOf(train) !== -1) {
                    const places_cells = row.querySelectorAll('.train_seats');
                    const seats = Array.from(places_cells).filter( function (cell) {
                        return parseInt(cell.innerText, 10) > 0
                    }); 
                    if (seats.length > 0) {
                        const count = seats.reduce((sum, value) => sum + parseInt(value.innerText, 10), 0);
                        if (!hasFreePlaces) {
                            emailjs.send('gmail', 'free_place', {
                                count,
                                date,
                                from,
                                to,
                                url
                            })
                            .then(
                                function(response) {
                                    hasFreePlaces = true; 
                                }, 
                                function(error) {
                                    hasFreePlaces = true;
                                }
                            );
                        }
                    }
                }
            })
        })
        .fail( function(error) {
            console.log(error)
        });
}