# Railway look

## Getting started
Install git  
Use in git bash `git clone <https-link>` to clone repo on your local machine  
Install [nodejs](https://nodejs.org/en/download/)  
From *rw_look* folder run next command:  
`npm install --global gulp-cli bower`  
Then do:  
`npm install`  
and  
`bower install`  
  
## Run application
Run command  
`gulp serve`
